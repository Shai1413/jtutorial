package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {

	public static void printLyrics(){
		
		String firstDay="A partridge in a pear tree";
		String secondDay="Two turtle doves";
		String thirdDay="Three french hens,";
		String fourthDay="Four calling birds,";
		String fifthDay="Five Golden Rings,";
		String sixthDay="Six geese a-laying,";
		String seventhDay="Seven swans a-swimming,";
		String eightDay="Eight maids a-milking,";
		String ninthDay="Nine ladies dancing,";
		String tenthDay="Ten lords a-leaping,";
		String eleventhDay="Eleven pipers piping,";
		String twelfthDay="Twelve Drummers drumming,";
		
		System.out.println("Twelve Days of Christmas\n");
		
		int days=1;
		for(; days<13; days++ ){
			String s = Integer.toString(days); 
			System.out.println("On the " + callDays(s) + " day of Christmas \nMy true love sent to me");
			
			switch(days){
			case 12:
				System.out.println(twelfthDay);
			case 11:
				System.out.println(eleventhDay);
			case 10:
				System.out.println(tenthDay);
			case 9:
				System.out.println(ninthDay);
			case 8:
				System.out.println(eightDay);
			case 7:
				System.out.println(seventhDay);
			case 6:
				System.out.println(sixthDay);
			case 5:
				System.out.println(fifthDay);
			case 4:
				System.out.println(fourthDay);
			case 3:
				System.out.println(thirdDay);
			case 2:
				System.out.println(secondDay + " and ");
			case 1:
				System.out.println(firstDay + "\n");
			}
		}
		
	}
	
	public static String callDays(String word){
				
		if("1".equals(word)){
			return"first";
		}
		else if("2".equals(word)){
			return "second";
		}
		else if("3".equals(word)){
			return "third";
		}
		else if("4".equals(word)){
			return "fourth";
		}
		else if("5".equals(word)){
			return "fifth";
		}
		else if("6".equals(word)){
			return "sixth";
		}
		else if("7".equals(word)){
			return "seventh";
		}
		else if("8".equals(word)){
			return "eighth";
		}
		else if("9".equals(word)){
			return "ninth";
		}
		else if("10".equals(word)){
			return "tenth";
		}
		else if("11".equals(word)){
			return "eleventh";
		}
		else if("12".equals(word)){
			return "twelfth";
		}		
		else
			return word;
	}
	
	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}

}
