package com.elavon.arraycollections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArrayCollectionsExercise {

    public static void main(final String[] args) {

        final String countryArray[] = { "United Kingdom", "Greece", "Japan", "Egypt", "Italy", "Canada", "Rome",
            "Australia", "Austria", "Maldives" };
        System.out.println("Size of Country Array: " + countryArray.length);

        final List<String> countryList = new ArrayList<>(Arrays.asList(countryArray));

        System.out.println("Size of Country List: " + countryList.size());
        countryList.add("Scotland");

        System.out.println("List of Countries is empty: " + countryList.isEmpty());
        System.out.println("New size of Country List: " + countryList.size());

        System.out.println("List of Countries contain Mongolia: " + countryList.contains("Mongolia"));
        System.out.println("List of Countries contain Scotland: " + countryList.contains("Scotland"));

        countryList.remove(countryList.size() - 1);

        // Display whether all of top countries are on my list
        String topCountries[] = { "Indonesia", "United Kingdom", "France", "Italy", "USA" };
        final List<String> countryTopList = new ArrayList<>(Arrays.asList(topCountries));

        if (countryList.containsAll(countryTopList)) {
            System.out.println("List of Countries contain top countries according to TripAdvisor: true\n");
        }
        else {
            System.out.println("List of Countries contain all top countries according to TripAdvisor: false");
        }

        System.out.println(countryList);

        // Map
        System.out.println();
        final Map<Integer, String> countryMap = new HashMap<>();

        Integer[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        String[] values = { "United Kingdom", "Greece", "Japan", "Egypt", "Italy", "Canada", "Rome", "Australia",
            "Austria", "Maldives" };

        if (keys.length == values.length) {
            for (int i = 0; i < values.length; i++) {
                countryMap.put(keys[i], values[i]);
            }
        }

        System.out.println(countryMap);
        System.out.println();

        // Sort
        Collections.sort(countryList);
        System.out.println(countryList);

        System.out.println();

        // Rank
        for (final Map.Entry<Integer, String> entry : countryMap.entrySet()) {
            System.out.println("Rank #" + entry.getKey() + ": " + entry.getValue());
        }

    }

}
