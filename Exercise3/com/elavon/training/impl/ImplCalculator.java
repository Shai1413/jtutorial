package com.elavon.training.impl;
import com.elavon.training.interf.*;

public class ImplCalculator implements AwesomeCalculator {

	@Override
	public int getSum(int augend, int addend) {
		return augend+addend;
	}
	
	@Override
	public int getSum(int... summands) {
		int sum=0;
		for (int i=0; i<summands.length; i++){
			sum+=summands[i];
		}
		return sum;
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		return minuend-subtrahend;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		return multiplicand*multiplier;
	}
	
	@Override
	public double getProduct(double... factors) {
		double product=1;
		for (int k=0; k<factors.length; k++){
			product=product*factors[k];
		}
		return product;
	}

	@Override
	public String getQuotientAndRemainder(int dividend, int divisor) {
		int quotient;
		int rem;
		quotient=dividend/divisor;
		rem=dividend%divisor;
		return (quotient + " remainder " + rem);
	}

	@Override
	public double toCelsius(int fahrenheit) {
		
		return (fahrenheit-32)*5/9;
	}

	@Override
	public double toFahrenheit(int celsius) {
		return celsius*9/5+32;
	}

	@Override
	public double toKilogram(double lbs) {
		return lbs/2.2;
	}

	@Override
	public double toPound(double kg) {
		return kg*2.2;
	}

	@Override
	public boolean isPalindrome(String str) {
		
		int strLength=5;
		for(int i=0;str.length()>i; i++){
				
			char currentLetterI = str.charAt(i);
			char currentLetter = str.charAt(strLength-1);
			
			if(currentLetterI != currentLetter){
				return false;
			}
			strLength--;
		}
		
		return true;
		

}

	

}
