package com.elavon.training.main;
import com.elavon.training.impl.ImplCalculator;

public class Main {

	public static void main(String[] args) {
		ImplCalculator calc=new ImplCalculator();

		System.out.println("Sum: "+ calc.getSum(100,355));
		//for arrays:
		System.out.println("Sum for Multiple Numbers: "+ calc.getSum(100,355,50));
		System.out.println("Difference: "+ calc.getDifference(455.5, 450));
		System.out.println("Product: "+ calc.getProduct(50, 50));
		//for arrays:
		System.out.println("Product for Multiple Numbers: "+ calc.getProduct(5.0, 5.0, 5.0, 10.0));
		System.out.println("Quotient and Remainder: "+ calc.getQuotientAndRemainder(45, 8));
		System.out.println("Celsius: "+ calc.toCelsius(212));
		System.out.println("Fahrenheit: "+ calc.toFahrenheit(100));
		System.out.println("Kilogram: "+ calc.toKilogram(154));
		System.out.println("Pound: "+ calc.toPound(53.37));
		System.out.println("Palindrome: "+ calc.isPalindrome("level"));
		
	}

}
