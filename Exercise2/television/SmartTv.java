package television;

public class SmartTv extends ColoredTV {

	protected String network;
	
	public SmartTv(String brandTV, String modelTV) {
		super(brandTV, modelTV);
		// TODO Auto-generated constructor stub
	}
	

//interface	
	interface NetworkConnection{
		public NetworkConnection connect(String name);
		public boolean connectionStatus();
	}
	
	class NetworkConnectionlmpl implements NetworkConnection{
		String connName;
		boolean networkStatusTv;

		@Override
		public boolean connectionStatus() {
			return networkStatusTv=true;
			
		}
		
		public NetworkConnection connect(String name){
			connName=name;
			networkStatusTv=true;
			return new NetworkConnectionlmpl();
			
		}
	}
	
	
//
	public String connectToNetwork(String networkName){
		return networkName;
	}
	
	public boolean hasNetworkConnection(boolean connectionStatus){
		return connectionStatus;
	}

	
	public static void main (String [] args){
		SmartTv samsungTv=new SmartTv("SAMSUNG", "SMTV1");
		
		System.out.println(samsungTv);
		
		samsungTv.mute();
		for(int i=0; i<3;i++)
		{
			samsungTv.brightnessUp();
		}
		for(int i=0; i<5;i++)
		{
			samsungTv.brightnessDown();
		}
		for(int i=0; i<3;i++)
		{
			samsungTv.contrastUp();
		}
		for(int i=0; i<5;i++)
		{
			samsungTv.contrastDown();
		}
		for(int i=0; i<3;i++)
		{
			samsungTv.pictureUp();
		}
	
			samsungTv.pictureDown();

	
		System.out.println(samsungTv);
		
		samsungTv.connectToNetwork("Test Network");
	}
	

}
