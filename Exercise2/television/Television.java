package television;

public class Television {
	
	protected String brand;
	protected String model;
	protected boolean powerOn;
	protected int channel;
	protected int volume;

	Television(String brandTV, String modelTV){

				brand=brandTV;
				model=modelTV;
				powerOn=false;
				channel=0;
				volume=5;
	}
	
	public void turnOn() {

		powerOn=true;
	}

	public void turnOff() {

		powerOn=false;
	}
	
	public void channelUp() {
		channel++;
	}

	public void channelDown() {
		channel--;
	}

	
	public void volumeUp() {
	
		volume++;
	}
	
	public void volumeDown() {
		volume--;
	}

	public String toString() {
	return brand + " " + model + " [ on:" + powerOn + ", channel:" + channel + ", volume:" + volume + " ]";
	}

	public static void main(String[] args) {
		Television TV= new Television("Andre Electronics","One");
		
		System.out.println(TV);
		TV.turnOn();
		
		int iChannelUp=0;
		int iVolumeDown=0;
		
		while(iChannelUp<5 )
		{
			TV.channelUp();
			iChannelUp++;
		}
		
		while(iVolumeDown<3 )
		{
			TV.volumeDown();
			iVolumeDown++;
		}
		
		TV.channelDown();
		TV.volumeUp();


		System.out.println(TV);


	}

}
