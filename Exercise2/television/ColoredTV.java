package television;

public class ColoredTV extends Television{

	protected int brightness;
	protected int contrast;
	protected int picture;

	ColoredTV(String brandTV, String modelTV) {
		super(brandTV, modelTV);
		brightness=50;
		contrast=50;
	 	picture=50;
	 	
	 	int iChannelUp=0;
		int iVolumeDown=0;
		
		while(iChannelUp<5 && iVolumeDown<3 )
		{
			super.channelUp();
			iChannelUp++;
		}
		
		while(iVolumeDown<3 )
		{
			super.volumeDown();
			iVolumeDown++;
		}
		super.channelDown();
		super.volumeUp();
	 	super.turnOn();
		super.turnOff();
	}

	public void  brightnessUp() {
		brightness++;
	}

	public void brightnessDown() {
	 	brightness--;
	}
	
	public void contrastUp() {
		contrast++;
	}

	public void contrastDown() {
	 	contrast--;
	}

	public void pictureUp() {
		picture++;	}

	public void pictureDown() {
	 	picture--;
	 }

	public int switchToChannel(int channelChange) {
		channel=channelChange;
	return channel;
	}

	public void mute() {
	volume=0;
	}
	
	public String toString() { 
	return super.toString()+" [ b:" + brightness + ", c:" + contrast + ", p:" + picture + " ]";
	}

	public static void main(String[] args) {
		
		Television bnwTV= new Television("Admiral","A1");
		ColoredTV sonyTV= new ColoredTV("SONY","S1");

		System.out.println(bnwTV);
		System.out.println(sonyTV);
		
		
		for(int i=0; i<5;i++){
			sonyTV.brightnessUp();
		}
			
		ColoredTV sharpTV= new ColoredTV("SHARP","SH1");

		sharpTV.mute();
		
		for(int i=0; i<10;i++){
			sharpTV.brightnessUp();
		}
		for(int i=0; i<8;i++){
			sharpTV.contrastDown();
		}
		for(int i=0; i<10;i++){
			sharpTV.pictureUp();
		}
		
		System.out.println(sharpTV);
	}

}
